<?php
/** @noinspection PhpSwitchStatementWitSingleBranchInspection */
/** @noinspection PhpSwitchCanBeReplacedWithMatchExpressionInspection */
/** @noinspection PhpUnused */
// 应用公共文件

/**
 * 格式时间/获取日期
 * @param string|int $f
 * @return string
 */
function get_date(string|int $f = ''): string
{
    $current_time = new DateTime();
    return match ($f) {
        '-u' => $current_time->format('Y-m-d H:i:s.u'),
        'k-u' => '['.$current_time->format('Y-m-d H:i:s.u').']',
        'k-' => '['.$current_time->format('Y-m-d H:i:s').']',
        '日' => $current_time->format('Y年m月d日 H时i分s秒'),
        '日u' => $current_time->format('Y年m月d日 H时i分s秒.u'),
        'm' => $current_time->format('Y-m'),
        'd' => $current_time->format('d'),
        'ymd' => $current_time->format('Y-m-d'),
        'ymdhms' => $current_time->format('YmdHis'),
        'ymdhmsu' => $current_time->format('YmdHisu'),
        default => $current_time->format('Y-m-d H:i:s'),
    };
}


/**
 * 写入日记
 * @param mixed $logs
 * @param string $log_type
 * @return void
 */
function write_log(mixed $logs,string $log_type='信息'): void
{
    try {
        $log_mode = 2; // 日记模式 1：日期目录 2：日期文件 3：单文件
        $no_log_type=['不重要','不记录'];
        $log_dir='log/';
        $log = json_encode_cn($logs) . "\n";
        $out = get_date('k-u').'['.$log_type.'] '. $log;
        if(defined('SHOW_LOG') && SHOW_LOG) echo $out;
        if(defined('LOG_WRITE') && !LOG_WRITE) return;
        if(in_array($log_type,$no_log_type)) return;

        switch ($log_mode) {
            case 1:
                $dir1=$log_dir.get_date('m').'/';
                if (!is_dir($dir1)) {
                    mkdir($dir1, 0777, true);
                }
                $name=$dir1.get_date('d').'_'.$log_type.'.log';
                break;
            case 2:
                $dir1=$log_dir;
                if (!is_dir($dir1)) {
                    mkdir($dir1, 0777, true);
                }
                $name=$dir1.get_date('ymd').'_'.$log_type.'.log';
                break;
            case 3:
                $dir1=$log_dir;
                if (!is_dir($dir1)) {
                    mkdir($dir1, 0777, true);
                }
                $name=$dir1.$log_type.'.log';
                break;
            default:
                $dir1=$log_dir;
                if (!is_dir($dir1)) {
                    mkdir($dir1, 0777, true);
                }
                $name=$dir1.get_date('ymd').'.log';
                break;
            }
        if (file_put_contents($name, $out, FILE_APPEND) === false) {
            throw new Exception("日志写入失败");
        }
    } catch (Exception $e) {
        echo "日志写入Error: " . $e->getMessage();
    }
}

/**
 * json编码，中文处理，json_encode
 * @param $data
 * @return bool|string
 */
function json_encode_cn($data): bool|string
{
    return json_encode($data, JSON_UNESCAPED_UNICODE);
}
